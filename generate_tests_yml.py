import yaml
import os
import argparse
import jinja2
from jinja2 import Environment, FileSystemLoader

if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument('-t', '--template')
	parser.add_argument('-v', '--variant')
	parser.add_argument('-d', '--dtbs')
	parser.add_argument('-l', '--labs')
	parser.add_argument('-b', '--boards')
	parser.add_argument('-o', '--output')

	args = parser.parse_args()

	with open(args.labs, 'r') as file:
		labs = yaml.safe_load(file)
	
	with open(args.boards, 'r') as file:
		boards = yaml.safe_load(file)

	environment = jinja2.Environment(loader=FileSystemLoader("./"))
	template = environment.get_template(args.template)

	board_id = 0
	bdata = {}

	for board in boards:
		data = boards[board]
		print("Generating files for '%s': %s" % (board, data))

		if not os.path.exists(args.dtbs + '/' + data['dtb']):
			print("DTB %s is missing, skip board" % data['dtb'])
			continue

		if "16K" in args.variant and not 'supports_16k' in data.keys():
			print("Kernel is built with 16K pages, CPU doesn't support it, skip board")
			continue

		if data["cdba"] is not None:
			for cdba in data["cdba"]:
				if cdba["name"] in labs:
					lab = labs[cdba["name"]]
					bdata[board_id] = {}
					bdata[board_id]["cdba"] = cdba["name"]
					bdata[board_id]["type"] = board
					bdata[board_id]["host"] = lab["host"]
					bdata[board_id]["id"] = cdba["board"]
					board_id = board_id + 1
				else:
					print("unknown lab %s" % cdba["name"])

	content = template.render(boards=bdata)
	with open(args.output, mode="w", encoding="utf-8") as f:
        	f.write(content)
