#/bin/bash

set -xe

# Usage: [project id] [package-name] [filename]

wget "${CI_API_V4_URL}/projects/${1}/packages?order_by=created_at&sort=desc" -O packages.json

# Get latest package version
ID=$(cat packages.json | jq -r ".[] | select(.name == \"${2}\") | .id" | head -n1)

if [ -z "${ID}" ]; then
	echo "Package ${1} from project ${2} not found" >&2
	exit 1
fi

wget "${CI_API_V4_URL}/projects/${1}/packages/${ID}" -O package.json

VERSION=$(cat package.json | jq -r '.version')

if [ -z "$VERSION" ]; then
	echo "Package ${1} from project ${2} version not found" >&2
	exit 1
fi

wget "${CI_API_V4_URL}/projects/${1}/packages/${ID}/package_files" -O files.json	

FILENAME=$(cat files.json | jq -r '.[].file_name' | grep -E "${3}")
if [ -z "$FILENAME" ]; then
	echo "Filaname ${3} of Package ${1} from project ${2} not found" >&2
	exit 1
fi

echo "${CI_API_V4_URL}/projects/${1}/packages/generic/${2}/${VERSION}/${FILENAME}"
