import os
import re
import argparse
from junit_xml import TestSuite, TestCase

kernel_log_timestamp = re.compile(r"\[[ ]+([0-9]+\.[0-9]+)\] (.+)")
testcase_signal = re.compile(r"<LAVA_SIGNAL_TESTCASE ([^>]+)>")
testcase_start = re.compile(r"<LAVA_SIGNAL_STARTTC ([^>]+)>")
testcase_end = re.compile(r"<LAVA_SIGNAL_ENDTC ([^>]+)>")

current_testcase = []

testcases = {}

def new_testcase(name):
	testcase = {}
	testcase["name"] = name
	testcase["status"] = "unknown"
	testcase["log"] = None
	testcase["measurement"] = None
	testcase["unit"] = None
	testcase["start"] = None
	testcase["end"] = None
	return testcase

def parse_testcase_start(m, time):
	global current_testcase
	global testcases
	name = m.group(1)

	print("Start test case %s" % name)
	
	if name not in testcases:
		testcases[name] = new_testcase(name)
		testcases[name]["start"] = time
		current_testcase.insert(0, testcases[name])
	else:
		print("Warning: start of existing testcase, ignoring")

def parse_testcase_end(m, time):
	global current_testcase
	global testcases
	found = False
	name = m.group(1)

	print("End of test case %s" % name)

	if len(current_testcase) == 0:
		print("Warning: end of testcase but no current testcases, ignoring")
		return

	for tc in current_testcase:
		if name == tc["name"]:
			found = True	

	if found == False:
		print("Warning: end of testcase not in current testcases, ignoring")
	
	while len(current_testcase) > 0:
		tc = current_testcase.pop(0)
		if tc["name"] == name:
			tc["end"] = time
			break

def parse_testcase(m):
	global current_testcase
	global testcases
	args = m.group(1).split()
	testcase = None

	print("Status of test case %s" % args)

	for arg in args:
		arg = arg.strip()
		
		(arg_type, arg_value) = arg.split("=")

		if arg_type == "TEST_CASE_ID":
			if arg_value not in testcases:
				testcases[arg_value] = new_testcase(arg_value)
			
			testcase = testcases[arg_value]
		elif arg_type == "RESULT":
			if testcase is None:
				print("Warning: malformed TESTCASE, TEST_CASE_ID should be first, ignoring")
				return
			else:
				testcase["status"] = arg_value
		elif arg_type == "MEASUREMENT":
			if testcase is None:
				print("Warning: malformed TESTCASE, TEST_CASE_ID should be first, ignoring")
				return
			else:
				testcase["measurement"] = arg_value
		elif arg_type == "UNITS":
			if testcase is None:
				print("Warning: malformed TESTCASE, TEST_CASE_ID should be first, ignoring")
				return
			else:
				testcase["unit"] = arg_value

def add_line_to_testcase(line):
	global current_testcase
	global testcases
	if len(current_testcase) > 0:
		if current_testcase[0]["log"] is None:
			current_testcase[0]["log"] = [line]
		else:
			current_testcase[0]["log"].append(line)

if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument('-l', '--boot-log')
	parser.add_argument('-x', '--xml')

	args = parser.parse_args()

	with open(args.boot_log, 'r', errors='replace') as file:
		boot_log = file.readlines()

	for line in boot_log:
		m_time = kernel_log_timestamp.search(line)
		if m_time is None:
			continue

		m_signal = testcase_signal.search(m_time.group(2))
		m_start = testcase_start.search(m_time.group(2))
		m_end = testcase_end.search(m_time.group(2))

		if m_signal is not None:
			parse_testcase(m_signal)
		elif m_start is not None:
			parse_testcase_start(m_start, m_time.group(1))
		elif m_end is not None:
			parse_testcase_end(m_end, m_time.group(1))
		else:
			add_line_to_testcase(m_time.group(2))

	tcs = []
	classname = None
	for testcase in testcases.keys():
		tc = None
		log = None
		elapsed = None
		measurement = None
		name = None
		status = None
		stdout = None
		output = None

		# Ignore top level testsuite case
		if testcase.startswith("testsuite-"):
			continue

		if testcases[testcase]["start"] is not None and \
			testcases[testcase]["end"] is not None:
			(start_s, start_us) = testcases[testcase]["start"].split(".")
			(end_s, end_us) = testcases[testcase]["end"].split(".")
			diff_s = int(end_s)-int(start_s)
			diff_us = int(end_us)-int(start_us)
			elapsed = diff_s + (float(diff_us) / 1000000)

		if testcases[testcase]["measurement"] is not None:
			measurement = testcases[testcase]["measurement"]
			if testcases[testcase]["unit"] is not None:
				measurement = measurement + " " + testcases[testcase]["unit"]

		if measurement is not None:
			name = measurement
			stdout = testcases[testcase]["log"]
			output = testcases[testcase]["log"]
			status = testcases[testcase]["status"]
		else:
			if testcases[testcase]["log"] is not None:
				name = testcases[testcase]["log"]
				stdout = None
				status = testcases[testcase]["status"]
				output = testcases[testcase]["log"]
			else:
				name = testcases[testcase]["status"]
				stdout = None
				status = None
				output = name

		tc = TestCase(classname=testcases[testcase]["name"], name=name, status=status, elapsed_sec=elapsed, stdout=stdout)

		if testcases[testcase]["status"] == "skip":
			tc.add_skipped_info(output=output)
		elif testcases[testcase]["status"] != "pass":
			tc.add_failure_info(output=output)

		tcs.append(tc)

	ts = TestSuite(name=classname, test_cases=tcs)

	with open(args.xml, 'w+') as file:
		file.write(TestSuite.to_xml_string([ts]))

