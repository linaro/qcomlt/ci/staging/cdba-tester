import yaml
import os
import argparse

if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument('-m', '--mkbootimg')
	parser.add_argument('-b', '--boards')
	parser.add_argument('-i', '--image')
	parser.add_argument('-I', '--image-gz')
	parser.add_argument('-f', '--initramfs')
	parser.add_argument('-d', '--dtbs')
	parser.add_argument('-o', '--output')

	args = parser.parse_args()

	with open(args.boards, 'r') as file:
		boards = yaml.safe_load(file)

	for board in boards:
		data = boards[board]
		print("Generating files for '%s': %s" % (board, data))

		if not os.path.exists(args.dtbs + '/' + data['dtb']):
			print("DTB %s is missing, skip board" % data['dtb'])
			continue

		if 'use-uncompressed-image' in data.keys() and data['use-uncompressed-image'] == 1:
			image_path = args.image
		else:
			image_path = args.image_gz

		if data['header-version'] == 0 or data['header-version'] == 1:
			cmdline = "cat %s %s > Image.gz+dtb\n" % (image_path, args.dtbs + '/' + data['dtb'])
			cmdline = cmdline + "%s --header_version %s --kernel Image.gz+dtb" % (args.mkbootimg, data['header-version'])
		else:
			cmdline = "%s --header_version %s --kernel %s --dtb %s" % (args.mkbootimg, data['header-version'], image_path, args.dtbs + '/' + data['dtb'])

		if 'base-addr' in data.keys():
			cmdline = cmdline + " --base %s" % data['base-addr']
		if 'ramdisk-offset' in data.keys():
			cmdline = cmdline + " --ramdisk_offset %s" % data['ramdisk-offset']
		if 'additional-args' in data.keys():
			cmdline = cmdline + " %s" % data['additional-args']

		cmdline = cmdline + " --cmdline \"%s\" --ramdisk %s --pagesize %s --output ${BOOTIMAGE_OUTPUT}\n" % (data['cmdline'], args.initramfs, data['pagesize'])

		if data['header-version'] == 0 or data['header-version'] == 1:
			cmdline = cmdline + "rm Image.gz+dtb\n"
			
		print(cmdline)

		with open(args.output + '/' + board + ".sh", 'w+') as file:
			file.write(cmdline)

