#!/bin/bash

set -e

#1: DT(s)
#2: modules path

for module in $(find $2 -name "*.ko"); do
	# Do not drop modules in drivers/soc/qcom
	is_soc_qcom=$(echo $module | grep "kernel/drivers/soc/qcom" | wc -l)
	if [ $is_soc_qcom -gt 0 ]; then
		continue
	fi

	of_aliases_count=$(modinfo -Falias $module | grep "of:N\*T\*C" | wc -l)
	if [ $of_aliases_count -lt 1 ]; then
		continue
	fi

	aliases=$(modinfo -Falias $module | grep "of:N\*T\*C" | sed "s,of:N\*T\*C,," | sed "s,C\*$,," | sort -u)
	found=0
	for compatible in $aliases; do
		found=$(( $found + $(grep -l $compatible $1 | wc -l) ))
	done
	if [ $found -lt 1 ]; then
		echo "ERASE $module"
		rm $module
	fi
done

version=$(basename $2/lib/modules/*)

depmod -b$2 $version