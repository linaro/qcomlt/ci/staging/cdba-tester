CDBA Linux Boot Tester pipeline
-------------------------------

This project provides a way to boot a kernel + an initramfs on a variety of ARM64 Qualcomm boards.

The pipeline can be run locally, or via a multi-project pipeline using the following CI variables:
- INITRAMFS_URL:
    Optional URL of cpio.xz initramfs to use for test, will use a pre-built initramfs when not provided.
- KERNEL_VERSION:
    Version of Linux (vX.X or next-X) (Mandatory)
- KERNEL_IMAGE_URL:
    URL on Linux ARM64 Image.gz (Mandatory)
- KERNEL_DTBS_URL:
    URL of Linux dtbs.tar.xz (Mandatory)
- KERNEL_MODULES_URL:
    URL of Linux modules.tar.xz (Mandatory)

To launch from another pipeline, you can use the following job definition:
```
test:
  stage: test
  needs:
    - job: build
      artifacts: false
  variables:
    KERNEL_VERSION: "${VERSION}"
    KERNEL_IMAGE_URL: "${IMAGE_URL}"
    KERNEL_DTBS_URL: "${DTBS_URL}"
    KERNEL_MODULES_URL: "${MODULES_URL}"
  trigger: 
    project: path/to/project/cdba-tester
    strategy: depend

```
